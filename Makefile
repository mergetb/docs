
PAGES = driver-dev-guide/index.html \
				internals-dev-guide/index.html \
				merge-api/index.html \
				portal-admin-manual/index.html \
				site-admin-manual/index.html \
				experimenter-guide/index.html

%.html: %.adoc
	asciidoctor $< -o $@

.PHONY: all
all: $(PAGES)

.PHONY: clean
clean:
	rm -f $(PAGES)
	
